import Phaser from 'phaser';

const GAME_WIDTH = 1280;
const GAME_HEIGHT = 720;

class Player extends Phaser.Physics.Arcade.Image {
    constructor(scene) {
        super(scene, -500, -500, 'space', 'playerShip1_blue.png');
        this.setScale(0.50);
        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
        this.lastFired = 0;
    }
}

class Enemy extends Phaser.Physics.Arcade.Image {
    constructor(scene,x, y) {
        super(scene, x, y, 'space', 'ufoRed.png');
        this.health = 10;
        this.setScale(0.50);
        this.scene.add.existing(this);
        this.scene.physics.add.existing(this);
        this.scene.events.on('update', this.update, this);
        this.healthText = this.scene.add.text(this.x, this.y - 45, `${this.health}`, { font: '20px Arial Bold' });
        this.healthText.setFill('#55eb34');
        this.initVelocity = Phaser.Math.Between(300,500);
        this.initDirection = Phaser.Math.Between(0,1) ? 0 -1 : 1;
        this.setVelocityX(this.initVelocity * this.initDirection);
    }

    handleDie()
    {
        this.setVelocityX(0);
        this.setVisible(false);
        this.healthText.setVisible(false);
        const sprite = this.scene.add.sprite(this.x,this.y,'kaboom');
        sprite.play({key:'explode', repeat: 0});
        this.destroy();
    }

    handleHit()
    {
        this.health--;
        if (this.health <= 0)
        {
            this.handleDie();
        }
    }

    changeDirection()
    {
        this.initDirection = this.initDirection * -1;
    }

    changeVelocity()
    {
        this.initVelocity = Phaser.Math.Between(300,500);
    }

    follow()
    {
        this.healthText.setX(this.x);
        this.healthText.setY(this.y - 45);
        this.healthText.setText(this.health);
    }

    update() {
        this.follow();
        if (this.x >= GAME_WIDTH - this.width/2 && this.initDirection === 1)
        {
            this.changeDirection();
            this.changeVelocity();
            this.setVelocityX(this.initVelocity * this.initDirection);

        }
        if (this.x <= this.width/2 && this.initDirection === -1)
        {
            this.changeDirection();
            this.changeVelocity();
            this.setVelocityX(this.initVelocity * this.initDirection);
        }
    }
}



class Bullet extends Phaser.Physics.Arcade.Image {
    constructor(scene) {
        super(scene, 0, 0, 'space', 'laserBlue01.png');
        this.setScale(0.50);
        this.speed = Phaser.Math.GetSpeed(500, 1);
    }

    fire(x, y) {
        this.setPosition(x, y - 50);

        this.setActive(true);
        this.setVisible(true);
    }

    update(time, delta) {
        this.y -= this.speed * delta;

        if (this.y < -50) {
            this.setActive(false);
            this.setVisible(false);
        }
    }
}

class MyGame extends Phaser.Scene {
    constructor() {
        super();
        this.cursors = null;
        this.player = null;
        this.bullets = null;
        this.badguys = [];
    }

    preload() {
        this.load.atlasXML('space', './assets/sheet.png', './assets/sheet.xml');
        this.load.image('starfield', './assets/starfield.png');
        this.load.spritesheet('kaboom', './assets/explode.png', {
            frameWidth: 128,
            frameHeight: 128
        });
    }

    create() {
        this.cursors = this.input.keyboard.createCursorKeys();

        //Create background
        const starfield = this.add.tileSprite(0, 0, 5120, 3072, 'starfield');
        starfield.setScale(0.5);

        //Create animation
        this.anims.create({
            key: 'explode',
            frames: this.anims.generateFrameNumbers('kaboom'),
            frameRate: 16
        });

        //Create Player
        this.player = new Player(this);//this.physics.add.image(-100,-100,'space','playerShip1_blue.png');
        this.player.x = GAME_WIDTH / 2;
        this.player.y = (GAME_HEIGHT - ((this.player.height * this.player.scale) / 2)) - 5;

        //Create bullet pool
        this.bullets = this.physics.add.group({
            classType: Bullet,
            maxSize: 10,
            runChildUpdate: true
        });

        //Create Bad guys
        this.badguys.push(new Enemy(this,GAME_WIDTH/2,50));
        this.badguys.push(new Enemy(this,GAME_WIDTH/2,200));
        this.badguys.push(new Enemy(this,GAME_WIDTH/2,350));

        //Handle BadGuy & Bullet Collisions
        this.physics.add.overlap(this.bullets, this.badguys, this.handleOverlap, null, this);
    }

    handleOverlap(badguy, bullet)
    {
        bullet.setActive(false);
        bullet.setVisible(false);
        bullet.x = -50;
        bullet.y = -50;
        badguy.handleHit();
    }

    update(time, delta) {
        if (this.cursors.left.isDown) {
            this.player.setVelocityX(-500);
        } else if (this.cursors.right.isDown) {
            this.player.setVelocityX(500);
        } else {
            this.player.setVelocity(0);
        }

        if (this.cursors.space.isDown && time > this.player.lastFired)
        {
            const bullet = this.bullets.get();

            if (bullet)
            {
                bullet.fire(this.player.x, this.player.y + (this.player.height * this.player.scale));

                this.player.lastFired = time + 200;
            }
        }
    }
}

const config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: GAME_WIDTH,
    height: GAME_HEIGHT,
    scene: MyGame,
    scale: {
        mode: Phaser.Scale.FIT
    },
    physics: {
        default: 'arcade',
        arcade: {
            debug: false,
            gravity: false
        },
    },
    mipmapFilter: "LINEAR_MIPMAP_LINEAR"
};

const game = new Phaser.Game(config);

