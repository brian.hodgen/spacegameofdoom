const path = require('path');

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyPlugin = require("copy-webpack-plugin");

module.exports =
    {
        mode: 'development',
        devtool: 'eval-cheap-module-source-map',
        optimization: {
            moduleIds: 'deterministic',
            removeAvailableModules: false,
            removeEmptyChunks: false,
            splitChunks: false
        },
        entry:
            {
                main: {
                    import: path.resolve(__dirname, './src/index.js'),
                    dependOn: 'phaser'
                },
                phaser: 'phaser'
            },
        output:
            {
                path: path.resolve(__dirname, './dist'),
                filename: '[name].bundle.js'
            },
        plugins: [
            new HtmlWebpackPlugin({
                title: 'Space Game Of Doom!',
                template: path.resolve(__dirname, './src/template.html'),
                filename: 'index.html'
            }),
            new CleanWebpackPlugin(),
            new webpack.HotModuleReplacementPlugin(),
            new CopyPlugin({
                patterns: [
                    { from: "src/assets", to: "assets" }
                ],
                options: {
                    concurrency: 100,
                },
            }),
            new webpack.DefinePlugin({
                CANVAS_RENDERER: JSON.stringify(true),
                WEBGL_RENDERER: JSON.stringify(true)
            })
        ],
        module: {
            rules: [
                {
                    test: /\.js$/,
                    include: path.resolve(__dirname, 'src'),
                    exclude: /node_modules/,
                    use: ['babel-loader']
                }
            ]
        },
        devServer: {
            historyApiFallback: true,
            contentBase: path.resolve(__dirname, './dist'),
            open: true,
            compress: true,
            hot: false,
            liveReload: true,
            port: 1337,
            writeToDisk: false
        }
    };
